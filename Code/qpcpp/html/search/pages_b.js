var searchData=
[
  ['pc_2dlint_2dplus_1811',['PC-Lint-Plus',['../lint.html',1,'ports_native']]],
  ['pedestrian_20ligtht_20controlled_20_28pelican_29_20crossing_1812',['PEdestrian LIgtht CONtrolled (PELICAN) Crossing',['../pelican.html',1,'exa_apps']]],
  ['pelican_2dgui_20for_20qt_1813',['PELICAN-GUI for Qt',['../qt_pelican-gui.html',1,'exa_qt']]],
  ['ports_1814',['Ports',['../ports.html',1,'']]],
  ['ports_20to_20third_2dparty_20os_1815',['Ports to Third-Party OS',['../ports_os.html',1,'ports']]],
  ['ports_20to_20third_2dparty_20rtos_1816',['Ports to Third-Party RTOS',['../ports_rtos.html',1,'ports']]],
  ['posix_1817',['POSIX',['../posix.html',1,'ports_os']]],
  ['posix_2dqv_1818',['POSIX-QV',['../posix-qv.html',1,'ports_os']]],
  ['preemptive_20_22dual_2dmode_22_20qxk_20kernel_1819',['Preemptive &quot;Dual-Mode&quot; QXK Kernel',['../arm-cm_qxk.html',1,'arm-cm']]],
  ['preemptive_20non_2dblocking_20qk_20kernel_1820',['Preemptive Non-Blocking QK Kernel',['../arm-cm_qk.html',1,'arm-cm']]]
];
