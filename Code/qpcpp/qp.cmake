set(qp_dir ${CMAKE_CURRENT_LIST_DIR})

set(qp_sources
    ${qp_dir}/src/qf/qep_hsm.cpp
    ${qp_dir}/src/qf/qep_msm.cpp
    ${qp_dir}/src/qf/qf_act.cpp
    ${qp_dir}/src/qf/qf_actq.cpp
    ${qp_dir}/src/qf/qf_defer.cpp
    ${qp_dir}/src/qf/qf_dyn.cpp
    ${qp_dir}/src/qf/qf_mem.cpp
    ${qp_dir}/src/qf/qf_ps.cpp
    ${qp_dir}/src/qf/qf_qact.cpp
    ${qp_dir}/src/qf/qf_qeq.cpp
    ${qp_dir}/src/qf/qf_time.cpp
    ${qp_dir}/src/qf/qf_qmact.cpp
)

if(MINGW)

    add_library(qp_port STATIC IMPORTED GLOBAL)

    set(qp_port_dir ports/win32-qv)
    set(qp_rel_dir rel)
    set(qp_dbg_dir dbg)
    set(qp_kernel qv)

    set_target_properties(qp_port PROPERTIES
        IMPORTED_LOCATION "${qp_dir}/${qp_port_dir}/${qp_rel_dir}/libqp.a"
        IMPORTED_LOCATION_DEBUG "${qp_dir}/${qp_port_dir}/${qp_dbg_dir}/libqp.a"
        IMPORTED_CONFIGURATIONS "RELEASE;DEBUG"
    )

    set(qp_sources
        ${qp_sources}
        ${qp_dir}/${qp_port_dir}/qf_port.cpp
    )

elseif(UNIX)

    set(qp_port_dir ports/posix-qv)

    set(qp_port_sources 
        ${qp_dir}/${qp_port_dir}/qf_port.cpp
    )

    set(qp_port_includes
        ${qp_dir}/${qp_port_dir}
        ${qp_dir}/include
        ${qp_dir}/src
    )

    add_library(qp_port STATIC ${qp_port_sources})

    target_include_directories(qp_port PUBLIC ${qp_port_includes})

    set(THREADS_PREFER_PTHREAD_FLAG ON)
    find_package(Threads REQUIRED)
    target_link_libraries(qp_port PRIVATE Threads::Threads)

    set(qp_port_dir ports/posix-qv)
    set(qp_rel_dir )

endif()


set(qp_includes
    ${qp_dir}/include
    ${qp_dir}/${qp_port_dir}
    ${qp_dir}/src
)

add_library(qp STATIC ${qp_sources})

target_link_libraries(qp PUBLIC qp_port)

target_include_directories(qp PUBLIC ${qp_includes})

target_compile_options(qp PUBLIC -w)