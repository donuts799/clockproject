set(sim_dir Applications/BSSyncronizer)

set(sim_sources
    ${sim_test_sources}
    ${ao_sources}
    ${hal_sources}
    #${qp_sources}
    ${sim_dir}/Main.cpp
)

set(sim_includes
    ${sim_dir}
    #${qp_includes}
    ${hal_includes}
    ${ao_includes}
    ${sim_test_includes}
)

add_executable(sim ${sim_sources})

target_include_directories(sim PUBLIC ${sim_includes})

target_link_libraries(sim PUBLIC qp)