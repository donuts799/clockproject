#include <iostream>
#include "Test.hpp"

int TestStarter::testsRunCount = 0;
int TestStarter::testsFailedCount = 0;

void printTestFinished(std::string module, std::string function, int line)
{
    std::cout << "Ok - " << module << ":" << function << ":" << std::to_string(line) << std::endl;
}

int main()
{
    std::cout << "------------------------------------------------" << std::endl;
    std::cout << TestStarter::testsRun() << " - Tests Completed Successfully" << std::endl;
    std::cout << TestStarter::testsFailed() << " - Tests Failed!!!" << std::endl;

    return 0;
}