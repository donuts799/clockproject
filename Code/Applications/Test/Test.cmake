set(test_dir Applications/Test)

set(test_sources
    ${hal_test_sources}
    ${test_dir}/Test.cpp
)

set(test_includes
    ${test_dir}
    ${hal_test_includes}
)

add_executable(test ${test_sources})

target_include_directories(test PUBLIC ${test_includes})