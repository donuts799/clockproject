#pragma once
//#include <cassert>
#include <iostream>

void printTestFinished(std::string module, std::string function, int line);

typedef void (*TestFunction)();

#define assert(value) ((value == true) ? "" : throw "Assertion Failed!")

class TestStarter
{
    static int testsRunCount;
    static int testsFailedCount;
public:
    TestStarter(TestFunction function)
    {
        try
        {
            function();
            testsRunCount++;
        }
        catch(char const* eStr)
        {
            std::cout << "Failed - ";
            std::cout << eStr << std::endl;
            testsFailedCount++;
        }
        catch(std::exception e)
        {
            std::cout << "Failed - ";
            std::cout << e.what() << std::endl;
        }
        catch(...)
        {
            std::cout << "Failed" << std::endl;
            testsFailedCount++;
        }
    }
    
    static int testsRun()
    {
        return testsRunCount;
    }

    static int testsFailed()
    {
        return testsFailedCount;
    }
};