#include "test.hpp"
#include "ECPServerAdapter.hpp"

using namespace ECP;

Packet testPacket;

void publishCallback(const Packet& p)
{
    testPacket = p;
}

void createServerAdapter()
{
    ServerAdapter a(publishCallback);
    // expect no failure
    printTestFinished("ECP",__func__,__LINE__);
}

void equalServerAdapter()
{
    ServerAdapter a(publishCallback);
    ServerAdapter b(publishCallback);
    assert(a == b);
    printTestFinished("ECP",__func__,__LINE__);
}


void differentCallback(const Packet& p)
{

}
void notEqualServerAdapter()
{
    ServerAdapter a(publishCallback);
    ServerAdapter b(differentCallback);
    assert(a != b);
    printTestFinished("ECP",__func__,__LINE__);
}

void publishOnServerAdapter()
{
    //             [version-]  [--------------------Command ID--------------]  [Data Length]  [--Data--]
    char data[] = {0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x02,    0x69, 0x42};
    ECP::Packet p = ECP::Packet(data);
    ServerAdapter a(publishCallback);
    testPacket = Packet();
    a.publish(p);
    assert(testPacket.data[0] == 0x69);
    // expect no failure
    printTestFinished("ECP",__func__,__LINE__);
}



TestStarter test_createServerAdapter((TestFunction)createServerAdapter);
TestStarter test_equalServerAdapter((TestFunction)equalServerAdapter);
TestStarter test_notEqualServerAdapter((TestFunction)equalServerAdapter);
TestStarter test_publishOnServerAdapter((TestFunction)publishOnServerAdapter);