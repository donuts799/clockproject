#include "ECP.hpp"

namespace ECP
{
    typedef void (*PublishCallback)(const Packet&);
    typedef uint32_t AdapterID;

    class ServerAdapter
    {
    private:
        PublishCallback callback = NULL;
        AdapterID id = NULL;
    public:
        ServerAdapter(PublishCallback callback);
        ~ServerAdapter();
        bool operator==(const ServerAdapter&) const;
        bool operator!=(const ServerAdapter&) const;
        void publish(const Packet&);
    };
}