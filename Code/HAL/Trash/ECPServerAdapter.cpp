#include "ECPServerAdapter.hpp"

namespace ECP
{
    ServerAdapter::ServerAdapter(PublishCallback callback)
    {
        this->callback = callback;
    }

    ServerAdapter::~ServerAdapter()
    {

    }

    bool ServerAdapter::operator==(const ServerAdapter& other) const
    {
        return this->callback == other.callback;
    }

    bool ServerAdapter::operator!=(const ServerAdapter& other) const
    {
        return !((*this) == other);
    }

    void ServerAdapter::publish(const Packet& packet)
    {
        callback(packet);
    }
}