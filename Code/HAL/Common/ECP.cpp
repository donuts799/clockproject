#include "ECP.hpp"

namespace ECP
{
    PacketData::PacketData()
    {
        raw = NULL;
        length = 0;
    }

    PacketData::PacketData(const char* const raw, PacketLength length)
    {
        this->length = length;
        if(length > 0)
        {
            this->raw = new char[length];
            memcpy(this->raw, raw, length);
        }
    }

    PacketData::~PacketData()
    {
        if(length != 0 && raw != NULL)
        {
            delete[] raw;
        }
    }

    void swap(PacketData& first, PacketData& second)
    {
        using std::swap;

        swap(first.raw,    second.raw);
        swap(first.length,  second.length);
    }

    PacketData::PacketData(const PacketData& copyFrom)
    {
        this->length = copyFrom.length;
        this->raw = new char[this->length];
        std::memcpy(this->raw, copyFrom.raw, this->length);
    }

    PacketData::PacketData(PacketData&& other) noexcept
    {
        swap(*this, other);
    }

    PacketData& PacketData::operator=(PacketData other)
    {
        swap(*this, other);

        return *this;
    }

    /*PacketData& PacketData::operator=(PacketData&& other) noexcept
    {
        swap(*this, other);

        return *this;
    }*/

    bool PacketData::operator==(const PacketData& other) const
    {
        if(length == other.length && 
            std::memcmp(raw, other.raw,length) == 0)
        {
            return true;
        }
        //implied else
        return false;
    }

    bool PacketData::operator!=(const PacketData& other) const
    {
        return !(*this == other);
    }

    PacketLength PacketData::getLength() const
    {
        return length;
    }

    char* PacketData::getRaw()
    {
        return raw;
    }

    char& PacketData::operator[](PacketLength index)
    {
        if(index >= length)
        {
            throw "Index out of bounds";
        }
        // implied else

        return raw[index];
    }

    Packet::Packet()
    {
        commandID = 0;
        version = 0;
        data = PacketData();
    }

    Packet::Packet(const char* const raw)
    {
        if(raw == 0)
        {
            throw "Null Pointer Exception";
        }
        version = ByteOrder::networkToHost16bit(*(uint16_t*)&raw[0]);
        
        commandID = ByteOrder::networkToHost64bit(*(uint64_t*)&raw[2]);

        PacketLength length = ByteOrder::networkToHost16bit(*(uint16_t*)&raw[10]);
        
        data = PacketData(&raw[12], length);
        
        if(version != 1)
        {
            throw "Packet version is unknown";
        }
        if(commandID == 0)
        {
            throw "Packet commandID is invalid";
        }
    }

    bool Packet::operator==(const Packet& other) const
    {
        if(other.commandID != this->commandID) return false;
        if(other.version != this->version) return false;
        if(other.data != this->data) return false;
        return true;
    }

    bool Packet::operator!=(const Packet& other) const
    {
        return !(*this == other);
    }

    uint64_t Packet::getCommandIDFromRaw(char* raw)
    {
        uint64_t commandID = ByteOrder::networkToHost64bit(*(uint64_t*)&raw[2]);
        return commandID;
    }
}