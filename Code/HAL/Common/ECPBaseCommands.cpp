#include "ECPBaseCommands.hpp"
#include "ByteOrder.hpp"

namespace ECP
{
    RegisterCommand_Packet::RegisterCommand_Packet(Packet raw) : Packet(raw)
    {
         if(data.getLength() != sizeof(PacketCommandID))
         {
             throw "Invalid packet data for RegisterCommand";
         }

         registerCommandID = ByteOrder::networkToHost64bit( *( (PacketCommandID*)data.getRaw() ) );
    }

    RegisterCommand_Packet::RegisterCommand_Packet(PacketCommandID idToRegister)
    {
        //             [version-]  [--------------------Command ID--------------]  [Data Length]  [--------------------Data--------------------]
        char data[] = {0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x08,    0x00, 0x00, 0x00, 0x00 ,0x00, 0x00, 0x00, 0x00};
        idToRegister = ByteOrder::hostToNetwork64bit(idToRegister);
        memcpy(data + 12, &idToRegister, sizeof(PacketCommandID));
        *this = ECP::Packet(data);
    }

    PacketCommandID RegisterCommand_Packet::getRegisterCommandID()
    {
        return registerCommandID;
    }
}