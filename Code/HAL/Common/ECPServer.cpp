#include "ECPServer.hpp"
#include "ECPBaseCommands.hpp"

namespace ECP
{
    ClientHandle Server::registerCallback(PublishCallback callback)
    {
        auto ret = registeredCommands.insert({callback, std::vector<PacketCommandID>()});
        if(ret.second == false)
        {
            throw "Callback already registered";
        }

        return callback;
    }

    bool Server::tryRegisterCommand(const Packet& packet, ClientHandle id)
    {
        if(packet.commandID != (PacketCommandID)COMMAND_ID::REGISTER_COMMAND)
            return false;

        RegisterCommand_Packet regPacket(packet);
        std::vector<PacketCommandID>& commandIDs = registeredCommands[id];

        // check if already registered
        if(std::find(commandIDs.begin(), commandIDs.end(), packet.commandID) != commandIDs.end())
        {
            // do nothing since already registered.
        }
        else
        {
            commandIDs.push_back(regPacket.getRegisterCommandID());
        }
        return true;
    }

    void Server::publish(const Packet& packet, ClientHandle id)
    {
        if(tryRegisterCommand(packet, id))
        {
            return; // if command was to this server, it is done.
        }
        // implied else
        for(auto pair : registeredCommands)
        {
            if(pair.first == id) // don't publish to itself
            {
                continue;
            }

            std::vector<PacketCommandID>& commandIDs = pair.second;
            PacketCommandID commandToPublish = packet.commandID;
            if(std::find(commandIDs.begin(), commandIDs.end(), commandToPublish) != commandIDs.end())
            {
                pair.first(packet);
            }
        }
    }
}