#include <chrono>
#include <ctime>
#include <sstream>
#include <iomanip>
#include "Date.hpp"

namespace Date
{
    Date::Date()
    {

    }

    Date Date::getCurrentDate()
    {
        using namespace std::chrono;
        Date returnValue;
        auto timepoint = system_clock::now();
        auto utcTime = system_clock::to_time_t(timepoint);
        auto now = std::localtime(&utcTime);
        returnValue.year        = 1900 + now->tm_year;
        returnValue.month       = static_cast<Month>(now->tm_mon);
        returnValue.day         = now->tm_yday;
        returnValue.monthday    = now->tm_mday;
        returnValue.weekday     = (Weekday)(now->tm_wday);
        returnValue.hour        = now->tm_hour;
        returnValue.minute      = now->tm_min;
        returnValue.second      = now->tm_sec;
        returnValue.millisecond = (duration_cast<milliseconds>(timepoint.time_since_epoch()).count()) % 1000;

        return returnValue;
    }

    String<50> Date::asDateTimeString() const
    {
        std::stringstream processingResult;
        processingResult << std::setw(4) << std::setfill('0') << this->year         << "-"
                         << std::setw(2) << std::setfill('0') << (int)this->month   << "-"
                         << std::setw(2) << std::setfill('0') << this->monthday     << "T"
                         << std::setw(2) << std::setfill('0') << this->hour         << ":"
                         << std::setw(2) << std::setfill('0') << this->minute       << ":"
                         << std::setw(2) << std::setfill('0') << this->second       << "."
                         << std::setw(3) << std::setfill('0') << this->millisecond;
        return String<50>(processingResult.str().c_str());
    }

    String<15> Date::getWeekdayString() const
    {
        switch(weekday)
        {
        case Weekday::SUNDAY:
            return String<15>("Sunday");
        case Weekday::MONDAY:
            return String<15>("Monday");
        case Weekday::TUESDAY:
            return String<15>("Tuesday");
        case Weekday::WEDNESDAY:
            return String<15>("Wednesday");
        case Weekday::THURSDAY:
            return String<15>("Thursday");
        case Weekday::FRIDAY:
            return String<15>("Friday");
        case Weekday::SATURDAY:
            return String<15>("Saturday");
        default:
            return String<15>("UNKNOWN");
        }
    }

    String<15> Date::getMonthString() const
    {
        switch(month)
        {
        case Month::JANUARY:
            return String<15>("January");
        case Month::FEBRUARY:
            return String<15>("February");
        case Month::MARCH:
            return String<15>("March");
        case Month::APRIL:
            return String<15>("April");
        case Month::MAY:
            return String<15>("May");
        case Month::JUNE:
            return String<15>("June");
        case Month::JULY:
            return String<15>("July");
        case Month::AUGUST:
            return String<15>("August");
        case Month::SEPTEMBER:
            return String<15>("September");
        case Month::OCTOBER:
            return String<15>("October");
        case Month::NOVEMBER:
            return String<15>("November");
        case Month::DECEMBER:
            return String<15>("December");
        default:
            return String<15>("UNKOWN");
        }
    }
}