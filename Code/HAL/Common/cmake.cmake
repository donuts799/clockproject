set(common_dir ${hal_dir}/Common)

set(hal_sources ${hal_sources}
    ${common_dir}/ECP.cpp
    ${common_dir}/ECPServer.cpp
    #${common_dir}/ECPServerAdapter.cpp
    ${common_dir}/ECPBaseCommands.cpp
    ${common_dir}/Date.cpp
    ${common_dir}/Debug.cpp
    ${common_dir}/Math.cpp
    ${common_dir}/String.cpp
)