#include <utility> // std::swap
#include "ByteOrder.hpp"

namespace ByteOrder
{
    uint16_t networkToHost16bit(uint16_t network)
    {
        std::swap(((char*)&network)[0], ((char*)&network)[1]);
        return network;
    }
    uint32_t networkToHost32bit(uint32_t network)
    {
        std::swap(((char*)&network)[0], ((char*)&network)[3]);
        std::swap(((char*)&network)[1], ((char*)&network)[2]);
        return network;
    }
    uint64_t networkToHost64bit(uint64_t network)
    {
        std::swap(((char*)&network)[0], ((char*)&network)[7]);
        std::swap(((char*)&network)[1], ((char*)&network)[6]);
        std::swap(((char*)&network)[2], ((char*)&network)[5]);
        std::swap(((char*)&network)[3], ((char*)&network)[4]);
        return network;
    }
    uint64_t hostToNetwork64bit(uint64_t host)
    {
        std::swap(((char*)&host)[0], ((char*)&host)[7]);
        std::swap(((char*)&host)[1], ((char*)&host)[6]);
        std::swap(((char*)&host)[2], ((char*)&host)[5]);
        std::swap(((char*)&host)[3], ((char*)&host)[4]);
        return host;
    }
}