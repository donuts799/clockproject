#include <String.hpp>
#include <stdint.h>

namespace Utilities
{
    typedef uint32_t MsUnit;
    void sleep(MsUnit msSleep);
}