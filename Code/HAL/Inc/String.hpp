#pragma once
#include <stdint.h>
#include <iostream>
#include <cstring>
#include <algorithm>

namespace Utilities
{
    typedef uint_fast16_t StringLength;

    class BaseString
    {
    public:
        virtual StringLength getLength() const = 0;
        virtual void add(const BaseString&) = 0;
        virtual void add(const char*) = 0;
        virtual const char* const getBuffer() const = 0;
        virtual char operator[](StringLength) const = 0;
        virtual void set(const BaseString& other) = 0;
        virtual void set(const char* other) = 0;
        virtual bool operator==(const BaseString& other) const = 0;
        virtual bool operator==(const char* other) const = 0;
    };

    template<StringLength MaxSize = 50>
    class String : public BaseString
    {
    public:
        static const StringLength MAX_STRING_SIZE = MaxSize;
        static const StringLength MAX_CSTRING_SIZE = MaxSize + 1; // To account for null terminator
        String();
        String(const char* inputString);
        String(const BaseString& inputString);
        StringLength getLength() const;
        char operator[](StringLength) const;
        void add(const BaseString&);
        void add(const char*);
        void set(const BaseString& other);
        void set(const char* other);
        bool operator==(const BaseString& other) const;
        bool operator==(const char* other) const;
        String<MaxSize> operator+(const BaseString&) const;
        String<MaxSize> operator+(const char* other) const;

        template <StringLength Size>// Just for friend function
        friend std::ostream& operator<<(std::ostream& os, String<Size> const& self);
        const char* const getBuffer() const;
    protected:
        StringLength length = 0;
        char buffer[MaxSize] = {0};
    };

    template<StringLength MaxSize>
    String<MaxSize>::String()
    {
        // Default constructor does not need to do anything
    }

    template<StringLength MaxSize>
    String<MaxSize>::String(const char* inputString)
    {
        StringLength i;
        for(i = 0; i < MAX_STRING_SIZE && inputString[i] != 0; i++)
        {
            buffer[i] = inputString[i];
        }

        length = i;
    }

    template<StringLength MaxSize>
    String<MaxSize>::String(const BaseString& inputString)
    {
        memcpy(&buffer[0], inputString.getBuffer(), std::min((StringLength)MAX_STRING_SIZE, inputString.getLength()));
        length = std::min(inputString.getLength(), (StringLength)MAX_STRING_SIZE);
    }

    template<StringLength MaxSize>
    StringLength String<MaxSize>::getLength() const
    {
        return length;
    }

    template<StringLength MaxSize>
    char String<MaxSize>::operator[](StringLength index) const
    {
        if(index > length)
        {
            return 0;
        }
        // implied else
        return buffer[index];
    }

    template<StringLength MaxSize>
    void String<MaxSize>::add(const BaseString& toAdd)
    {
        memcpy(&buffer[length], toAdd.getBuffer(), std::min(MAX_STRING_SIZE - length, toAdd.getLength()));
        length = std::min(length + toAdd.getLength(), (StringLength)MAX_STRING_SIZE);
    }

    template<StringLength MaxSize>
    void String<MaxSize>::add(const char* toAdd)
    {
        StringLength i;
        for(i = 0; i + length < MAX_STRING_SIZE && toAdd[i] != 0; i++)
        {
            buffer[i + length] = toAdd[i];
        }

        length = i + length;
    }

    template<StringLength MaxSize>
    void String<MaxSize>::set(const BaseString& from)
    {
        memcpy(&buffer[0], from.getBuffer(), std::min((StringLength)MAX_STRING_SIZE, from.getLength()));
        length = std::min(from.getLength(), (StringLength)MAX_STRING_SIZE);
    }

    template<StringLength MaxSize>
    void String<MaxSize>::set(const char* toAdd)
    {
        StringLength i;
        for(i = 0; i < MAX_STRING_SIZE && toAdd[i] != 0; i++)
        {
            buffer[i] = toAdd[i];
        }

        length = i;
    }

    template<StringLength MaxSize>
    bool String<MaxSize>::operator==(const BaseString& other) const
    {
        if(length != other.getLength())
        {
            return false;
        }

        for(int i = 0; i < length; i++)
        {
            if(buffer[i] != other[i])
            {
                return false;
            }
        }

        return true;
    }

    template<StringLength MaxSize>
    bool String<MaxSize>::operator==(const char* other) const
    {
        for(int i = 0; i < length; i++)
        {
            if(buffer[i] != other[i] || other[i] == 0)
            {
                return false;
            }
        }

        if(other[length] != 0)
        {
            // Other c_str didn't end yet
            return false;
        }

        return true;
    }

    template<StringLength MaxSize>
    String<MaxSize> String<MaxSize>::operator+(const BaseString& other) const
    {
        String<MaxSize> returnString(*this);
        returnString.add(other);
        return returnString;
    }

    template<StringLength MaxSize>
    String<MaxSize> String<MaxSize>::operator+(const char* other) const
    {
        String<MaxSize> returnString(*this);
        returnString.add(other);
        return returnString;
    }

    template<StringLength MaxSize>
    std::ostream& operator<<(std::ostream& os, String<MaxSize> const& self)
    {
        for(size_t i = 0; i < self.length; i++)
        {
            os << self.buffer[i];
        }
        return os;
    }

    template<StringLength MaxSize>
    const char* const String<MaxSize>::getBuffer() const
    {
        return buffer;
    }
}