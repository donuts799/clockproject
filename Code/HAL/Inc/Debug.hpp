#pragma once
#include <cstdint>
#include <string>

namespace Debug
{
    enum class DebugLevel
    {
        INFORMATIONAL,
        WARNING,
        ERROR
    };

    void print(std::string toPrint);
}


