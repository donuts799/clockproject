#pragma once
#include <cstdint>
#include <cstring>
#include <iostream>
#include "ByteOrder.hpp"

namespace ECP
{
    enum class COMMAND_ID
    {
        INVALID_MIN = 0,
        REGISTER_COMMAND = 1,
        INVALID_MAX
    };

    typedef uint16_t PacketLength;
    typedef uint16_t PacketVersion;
    typedef uint64_t PacketCommandID;
    
    const PacketCommandID START_OF_CUSTOM_COMMAND = 1024;

    class PacketData
    {
        char* raw = NULL;
        PacketLength length = 0;
    public:
        PacketData();
        PacketData(const char* const raw, PacketLength length);
        ~PacketData();
        friend void swap(PacketData& first, PacketData& second);
        PacketData(const PacketData& other);
        PacketData(PacketData&& other) noexcept;
        PacketData& operator=(PacketData);
        /*PacketData& operator=(PacketData&&) noexcept;*/ // TODO: how???
        bool operator==(const PacketData&) const;
        bool operator!=(const PacketData&) const;
        PacketLength getLength() const;
        char* getRaw();
        char& operator[](PacketLength index);
    };

    class Packet
    {
    public:
        PacketData data;
        PacketCommandID commandID = 0;
        PacketVersion version = 0;
    public:
        Packet();
        Packet(const char* const raw);
        bool operator==(const Packet&) const;
        bool operator!=(const Packet&) const;
        static PacketCommandID getCommandIDFromRaw(char* raw);
    };
}
