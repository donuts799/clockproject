#include <vector>
#include <map>
#include "ECP.hpp"

namespace ECP
{
    typedef void (*PublishCallback)(const Packet&);
    typedef PublishCallback ClientHandle;

    class Server
    {
    private:
        std::map<PublishCallback, std::vector<PacketCommandID>> registeredCommands;
        bool tryRegisterCommand(const Packet&, ClientHandle);
    public:
        ClientHandle registerCallback(PublishCallback);
        void publish(const Packet&, ClientHandle);
    };
}