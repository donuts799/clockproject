#include "ECP.hpp"

namespace ECP
{
    class RegisterCommand_Packet : public Packet
    {
    private:
        PacketCommandID registerCommandID;
    public:
        RegisterCommand_Packet(Packet raw);
        RegisterCommand_Packet(PacketCommandID idToRegister);
        PacketCommandID getRegisterCommandID();
    };
}