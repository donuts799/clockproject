
namespace Math
{
    template<typename CompareType>
    bool isWithinPercent(CompareType reference, CompareType compareFrom, float percent)
    {
        // Greater than "percent" less than reference OR smaller than "percent" greater than reference
        return compareFrom > (reference - (CompareType)(reference * (percent/100))) &&
                compareFrom < (reference + (CompareType)(reference * (percent/100)));
    }
}