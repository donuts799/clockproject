#pragma once
#include <cstdint>
#include <string>
#include "Utilities.hpp"

using namespace Utilities;

namespace Date
{
    typedef uint16_t Year;
    typedef uint16_t Day;
    typedef uint16_t Monthday;
    typedef uint16_t Hour;
    typedef uint16_t Minute;
    typedef uint16_t Second;
    typedef uint32_t Millisecond;
    typedef uint32_t Microsecond;

    enum class Month
    {
        JANUARY     = 1,
        FEBRUARY    = 2,
        MARCH       = 3,
        APRIL       = 4,
        MAY         = 5,
        JUNE        = 6,
        JULY        = 7,
        AUGUST      = 8,
        SEPTEMBER   = 9,
        OCTOBER     = 10,
        NOVEMBER    = 11,
        DECEMBER    = 12,
    };

    enum class Weekday
    {
        SUNDAY      = 0,
        MONDAY      = 1,
        TUESDAY     = 2,
        WEDNESDAY   = 3,
        THURSDAY    = 4,
        FRIDAY      = 5,
        SATURDAY    = 6
    };

    class Date
    {
    public:
        Year year               = 0;
        Month month             = Month::JANUARY;
        Day day                 = 0;
        Monthday monthday       = 0;
        Weekday weekday         = Weekday::SUNDAY;
        Hour hour               = 0;
        Minute minute           = 0;
        Second second           = 0;
        Millisecond millisecond = 0;
        Microsecond microsecond = 0;

    public:
        Date();

        String<50> asDateTimeString() const;

        static Date getCurrentDate();

        String<15> getWeekdayString() const;

        String<15> getMonthString() const;
    };
}