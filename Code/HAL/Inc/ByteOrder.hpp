#pragma once
#include <cstdint>

namespace ByteOrder
{
    uint16_t networkToHost16bit(uint16_t network);
    uint32_t networkToHost32bit(uint32_t network);
    uint64_t networkToHost64bit(uint64_t network);
    uint64_t hostToNetwork64bit(uint64_t host);
}