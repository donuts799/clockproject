#include "Utilities.hpp"
#include <time.h>
#include <errno.h>


namespace Utilities
{
    void sleep(MsUnit msSleep)
    {
        struct timespec ts;
        int result;

        ts.tv_sec = msSleep / 1000;
        ts.tv_nsec = (msSleep % 1000) * 1000000;
        
        do
        {
            result = nanosleep(&ts, &ts);
        } while (result && errno == EINTR);
    }
}