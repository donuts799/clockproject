#include "Test.hpp"
#include "ECP.hpp"
#include <iostream>
using namespace ECP;


void createPacketData()
{
    const PacketLength dataLength = 5;
    char data[dataLength] = {1,2,3,4,5};
    PacketData a = PacketData(data, dataLength);
    // expect no failure
    printTestFinished("ECP",__func__,__LINE__);
}

void comparePacketDataEqual()
{
    const PacketLength length1 = 5;
    const PacketLength length2 = 5;
    char dataA[length1] = {1,2,3,4,5};
    char dataB[length2] = {1,2,3,4,5};
    PacketData a = PacketData(dataA, length1);
    PacketData b = PacketData(dataB, length2);
    assert(a == b);
    printTestFinished("ECP",__func__,__LINE__);
}

void comparePacketDataNotEqual()
{
    const PacketLength length1 = 5;
    const PacketLength length2 = 5;
    char dataA[length1] = {1,2,3,4,5};
    char dataB[length2] = {1,2,3,4,6};
    PacketData a = PacketData(dataA, length1);
    PacketData b = PacketData(dataB, length2);
    assert(a != b);
    printTestFinished("ECP",__func__,__LINE__);
}

void getPacketDataLength()
{
    const PacketLength dataLength = 5;
    char data[dataLength] = {1,2,3,4,5};
    PacketData a = PacketData(data, dataLength);

    assert(a.getLength() == 5);
    printTestFinished("ECP",__func__,__LINE__);
}

void getPacketDataRaw()
{
    const PacketLength dataLength = 5;
    char data[dataLength] = {1,2,3,4,5};
    PacketData a = PacketData(data, dataLength);

    assert(a.getRaw()[4] == 5);
    printTestFinished("ECP",__func__,__LINE__);
}

void getPacketDataSubscriptOperator()
{
    const PacketLength dataLength = 5;
    char data[dataLength] = {1,2,69,42,5};
    PacketData a = PacketData(data, dataLength);

    assert(a[0] == 1);
    assert(a[1] == 2);
    assert(a[2] == 69);
    assert(a[3] == 42);
    assert(a[4] == 5);
    printTestFinished("ECP",__func__,__LINE__);
}

void createEmptyPacket()
{
    ECP::Packet a;
    // expect no failure
    printTestFinished("ECP",__func__,__LINE__);
}

void createPacket_WithData()
{
    //             [version-]  [--------------------Command ID--------------]  [Data Length]  [--Data--]
    char data[] = {0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x02,    0x69, 0x42};
    ECP::Packet p = ECP::Packet(data);
    
    // std::cout << "version: " << p.version << " commandID: " << p.commandID << " dataLength: " << p.dataLength << " data[0]: " << (int)p.data[0] << std::endl;
    assert(p.commandID == 1);
    assert(p.data.getLength() == 2);
    assert(p.data.getRaw()[0] == 0x69);
    printTestFinished("ECP",__func__,__LINE__);
}

void getCommandID()
{
    //             [version-]  [--------------------Command ID--------------]  [Data Length]  [--Data--]
    char data[] = {0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x45, 0x00, 0x02,    0x69, 0x42};
    auto id = ECP::Packet::getCommandIDFromRaw(data);
    assert(id == 69);
    printTestFinished("ECP",__func__,__LINE__);
}

void equalPackets()
{
    //             [version-]  [--------------------Command ID--------------]  [Data Length]  [--Data--]
    char data[] = {0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x45, 0x00, 0x02,    0x69, 0x42};
    auto first = ECP::Packet(data);
    auto second = ECP::Packet(data);
    assert(first == second);
    printTestFinished("ECP",__func__,__LINE__);
}

void notEqualPackets()
{
    //             [version-]  [--------------------Command ID--------------]  [Data Length]  [--Data--]
    char data1[] = {0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x45, 0x00, 0x02,    0x69, 0x42};
    auto first = ECP::Packet(data1);
    //             [version-]  [--------------------Command ID--------------]  [Data Length]  [--Data--]
    char data2[] = {0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x45, 0x00, 0x02,    0x69, 0x43};
    auto second = ECP::Packet(data2);
    assert(first != second);
    printTestFinished("ECP",__func__,__LINE__);
}

void getLength()
{
    //             [version-]  [--------------------Command ID--------------]  [Data Length]  [--Data--]
    char data[] = {0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x45, 0x00, 0x02,    0x69, 0x42};
    auto packet = ECP::Packet(data);
    assert(packet.data.getLength() == 2);
    printTestFinished("ECP",__func__,__LINE__);
}

TestStarter test_createPacketData((TestFunction)createPacketData);
TestStarter test_getPacketDataLength((TestFunction)getPacketDataLength);
TestStarter test_getPacketDataRaw((TestFunction)getPacketDataRaw);
TestStarter test_comparePacketDataEqual((TestFunction)comparePacketDataEqual);
TestStarter test_comparePacketDataNotEqual((TestFunction)comparePacketDataNotEqual);
TestStarter test_getPacketDataSubscriptOperator((TestFunction)getPacketDataSubscriptOperator);
TestStarter test_createEmptyPacket((TestFunction)createEmptyPacket);
TestStarter test_createPacket_WithData((TestFunction)createPacket_WithData);
TestStarter test_getCommandID((TestFunction)getCommandID);
TestStarter test_equalPackets((TestFunction)equalPackets);
TestStarter test_notEqualPackets((TestFunction)notEqualPackets);
TestStarter test_getLength((TestFunction)getLength);