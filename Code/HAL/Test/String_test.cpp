#include "Test.hpp"
#include "String.hpp"
#include <sstream>

using namespace Utilities;

void create_String()
{
    String a;
    printTestFinished("Utilities",__func__,__LINE__);
}

void verifyLength_String_empty()
{
    String a;
    assert(a.getLength() == 0);
    printTestFinished("Utilities",__func__,__LINE__);
}

void verifyLength_String_10Chars()
{
    String a("0123456789");
    assert(a.getLength() == 10);
    printTestFinished("Utilities",__func__,__LINE__);
}

void operatorAtIndex_String_Valid()
{
    String a("test:{69}");
    assert(a[6] == '6');
    assert(a[7] == '9');
    printTestFinished("Utilities",__func__,__LINE__);
}

void add_StringToEmpty()
{
    String a;
    String toAdd("b");
    a.add(toAdd);
    assert(1 == a.getLength());
    assert('b' == a[0]);
    printTestFinished("Utilities",__func__,__LINE__);
}

void add_StringToNotEmpty()
{
    String a("a");
    String toAdd("b");
    a.add(toAdd);
    assert(2 == a.getLength());
    assert('b' == a[1]);
    printTestFinished("Utilities",__func__,__LINE__);
}

void add_ExceedMax()
{
    String<10> a("012345");
    String<10> b("6789X");
    a.add(b);

    assert(10 == a.getLength());
    assert('0' == a[0]);
    assert('9' == a[9]);
    printTestFinished("Utilities",__func__,__LINE__);
}

void add_CStringToEmpty()
{
    String a;
    a.add("b");
    assert(1 == a.getLength());
    assert('b' == a[0]);
    printTestFinished("Utilities",__func__,__LINE__);
}

void add_CStringToNotEmpty()
{
    String a("a");
    String toAdd("b");
    a.add("b");
    assert(2 == a.getLength());
    assert('b' == a[1]);
    printTestFinished("Utilities",__func__,__LINE__);
}

void add_CStringExceedMax()
{
    String<10> a("012345");
    String<10> b("6789X");
    a.add("6789X");

    assert(10 == a.getLength());
    assert('0' == a[0]);
    assert('9' == a[9]);
    printTestFinished("Utilities",__func__,__LINE__);
}

void operatorShift_IntoStringStream()
{
    String<10> from("1234567890");
    std::stringstream to;
    to << from;
    assert(to.str() == "1234567890");
    printTestFinished("Utilities",__func__,__LINE__);
}

void set_ToEmpty()
{
    String<10> to("1234567890");
    String<10> from;
    to.set(from);
    assert(0 == to.getLength());
    printTestFinished("Utilities",__func__,__LINE__);
}

void set_ToNotEmpty()
{
    String<10> to;
    String<10> from("1234567890");
    to.set(from);
    assert(10 == to.getLength());
    assert('1' == to[0]);
    assert('0' == to[9]);
    printTestFinished("Utilities",__func__,__LINE__);
}

void set_CStringToEmpty()
{
    String<10> to("1234567890");
    const char* from = "";
    to.set(from);
    assert(0 == to.getLength());
    printTestFinished("Utilities",__func__,__LINE__);
}

void set_CStringToNotEmpty()
{
    String<10> to;
    const char* from = "1234567890";
    to.set(from);
    assert(10 == to.getLength());
    assert('1' == to[0]);
    assert('0' == to[9]);
    printTestFinished("Utilities",__func__,__LINE__);
}

void compare_Equal()
{
    String<10> a("Equal");
    String<10> b("Equal");

    assert(a == b);
    printTestFinished("Utilities",__func__,__LINE__);
}

void compare_NotEqualSameLength()
{
    String<10> a("Equal");
    String<10> b("Equa!");

    assert(!(a == b));
    printTestFinished("Utilities",__func__,__LINE__);
}

void compare_NotEqualSmaller()
{
    String<10> a("Equal");
    String<10> b("Equal1");

    assert(!(a == b));
    printTestFinished("Utilities",__func__,__LINE__);
}

void compare_NotEqualLarger()
{
    String<10> a("Equal1");
    String<10> b("Equal");

    assert(!(a == b));
    printTestFinished("Utilities",__func__,__LINE__);
}

void compare_CStringEqual()
{
    String<10> a("Equal");
    const char* b = "Equal";

    assert(a == b);
    printTestFinished("Utilities",__func__,__LINE__);
}

void compare_CStringNotEqualSameLength()
{
    String<10> a("Equal");
    const char* b = "Equa!";

    assert(!(a == b));
    printTestFinished("Utilities",__func__,__LINE__);
}

void compare_CStringNotEqualSmaller()
{
    String<10> a("Equal");
    const char* b = "Equal1";

    assert(!(a == b));
    printTestFinished("Utilities",__func__,__LINE__);
}

void compare_CStringNotEqualLarger()
{
    String<10> a("Equal1");
    const char* b = "Equal";

    assert(!(a == b));
    printTestFinished("Utilities",__func__,__LINE__);
}

void operatorAdd_StringToEmpty()
{
    String a;
    String toAdd("b");
    String result = a+toAdd;
    assert(1 == result.getLength());
    assert('b' == result[0]);
    printTestFinished("Utilities",__func__,__LINE__);
}

void operatorAdd_StringToNotEmpty()
{
    String a("a");
    String toAdd("b");
    String result = a+toAdd;
    assert(2 == result.getLength());
    assert('b' == result[1]);
    printTestFinished("Utilities",__func__,__LINE__);
}

void operatorAdd_ExceedMax()
{
    String<10> a("012345");
    String<10> toAdd("6789X");
    String result = a+toAdd;

    assert(10 == result.getLength());
    assert('0' == result[0]);
    assert('9' == result[9]);
    printTestFinished("Utilities",__func__,__LINE__);
}

void operatorAdd_CStringStringToEmpty()
{
    String a;
    const char* toAdd = "b";
    String result = a+toAdd;
    assert(1 == result.getLength());
    assert('b' == result[0]);
    printTestFinished("Utilities",__func__,__LINE__);
}

void operatorAdd_CStringStringToNotEmpty()
{
    String a("a");
    const char* toAdd ="b";
    String result = a+toAdd;
    assert(2 == result.getLength());
    assert('b' == result[1]);
    printTestFinished("Utilities",__func__,__LINE__);
}

void operatorAdd_CStringExceedMax()
{
    String<10> a("012345");
    const char* toAdd = "6789X";
    String result = a+toAdd;

    assert(10 == result.getLength());
    assert('0' == result[0]);
    assert('9' == result[9]);
    printTestFinished("Utilities",__func__,__LINE__);
}

TestStarter test_String((TestFunction)create_String);
TestStarter test_verifyLength_String_empty((TestFunction)verifyLength_String_empty);
TestStarter test_verifyLength_String_10Chars((TestFunction)verifyLength_String_10Chars);
TestStarter test_operatorAtIndex_String_Valid((TestFunction)operatorAtIndex_String_Valid);
TestStarter test_add_StringToEmpty((TestFunction)add_StringToEmpty);
TestStarter test_add_StringToNotEmpty((TestFunction)add_StringToNotEmpty);
TestStarter test_add_ExceedMax((TestFunction)add_ExceedMax);
TestStarter test_add_CStringToEmpty((TestFunction)add_CStringToEmpty);
TestStarter test_add_CStringToNotEmpty((TestFunction)add_CStringToNotEmpty);
TestStarter test_add_CStringExceedMax((TestFunction)add_CStringExceedMax);
TestStarter test_operatorShift_IntoStringStream((TestFunction)operatorShift_IntoStringStream);
TestStarter test_set_ToEmpty((TestFunction)set_ToEmpty);
TestStarter test_set_ToNotEmpty((TestFunction)set_ToNotEmpty);
TestStarter test_set_CStringToEmpty((TestFunction)set_CStringToEmpty);
TestStarter test_set_CStringToNotEmpty((TestFunction)set_CStringToNotEmpty);
TestStarter test_compare_Equal((TestFunction)compare_Equal);
TestStarter test_compare_NotEqualSameLength((TestFunction)compare_NotEqualSameLength);
TestStarter test_compare_NotEqualSmaller((TestFunction)compare_NotEqualSmaller);
TestStarter test_compare_NotEqualLarger((TestFunction)compare_NotEqualLarger);
TestStarter test_compare_CstringEqual((TestFunction)compare_CStringEqual);
TestStarter test_compare_CstringNotEqualSameLength((TestFunction)compare_CStringNotEqualSameLength);
TestStarter test_compare_CstringNotEqualSmaller((TestFunction)compare_CStringNotEqualSmaller);
TestStarter test_compare_CstringNotEqualLarger((TestFunction)compare_CStringNotEqualLarger);
TestStarter test_operatorAdd_StringToEmpty((TestFunction)operatorAdd_StringToEmpty);
TestStarter test_operatorAdd_StringToNotEmpty((TestFunction)operatorAdd_StringToNotEmpty);
TestStarter test_operatorAdd_ExceedMax((TestFunction)operatorAdd_ExceedMax);
TestStarter test_operatorAdd_CStringStringToEmpty((TestFunction)operatorAdd_CStringStringToEmpty);
TestStarter test_operatorAdd_CStringStringToNotEmpty((TestFunction)operatorAdd_CStringStringToNotEmpty);
TestStarter test_operatorAdd_CStringExceedMax((TestFunction)operatorAdd_CStringExceedMax);
