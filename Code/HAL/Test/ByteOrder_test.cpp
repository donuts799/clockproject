#include "Test.hpp"
#include "ByteOrder.hpp"
#include <iostream>

void networkToHost16bit()
{
    uint16_t hostData;
    char networkData[] = {0x00, 0x45}; // assumes host is little endian for test
    hostData = ByteOrder::networkToHost16bit(*(uint16_t*)(networkData));
    assert(hostData == 69);
    printTestFinished("ByteOrder",__func__,__LINE__);
}

void networkToHost32bit()
{
    uint32_t hostData;
    char networkData[] = {0x00, 0x00, 0x00, 0x45}; // assumes host is little endian for test
    hostData = ByteOrder::networkToHost32bit(*(uint32_t*)(networkData));
    assert(hostData == 69);
    printTestFinished("ByteOrder",__func__,__LINE__);
}

void networkToHost64bit()
{
    uint64_t hostData;
    char networkData[] = {0x00, 0x000, 0x00, 0x00, 0x00, 0x00, 0x00, 0x45}; // assumes host is little endian for test
    hostData = ByteOrder::networkToHost64bit(*(uint64_t*)(networkData));
    assert(hostData == 69);
    printTestFinished("ByteOrder",__func__,__LINE__);
}

void hostToNetwork64bit()
{
    uint64_t networkData;
    networkData = ByteOrder::hostToNetwork64bit(0x69);
    char networkCompare[] = {0x00, 0x000, 0x00, 0x00, 0x00, 0x00, 0x00, 0x69};
    assert(networkData == *(uint64_t*)networkCompare);
    printTestFinished("ByteOrder",__func__,__LINE__);
}

TestStarter test_networkToHost16bit((TestFunction)networkToHost16bit);
TestStarter test_networkToHost32bit((TestFunction)networkToHost32bit);
TestStarter test_networkToHost64bit((TestFunction)networkToHost64bit);
TestStarter test_hostToNetwork64bit((TestFunction)hostToNetwork64bit);