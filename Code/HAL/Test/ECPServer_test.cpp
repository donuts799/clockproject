#include "Test.hpp"
#include "ECPServer.hpp"
#include "ECPBaseCommands.hpp"

using namespace ECP;

char packetData = 0;

void createServer()
{
    Server server;
    // expect no failure
    printTestFinished("ECP",__func__,__LINE__);
}

void ecpCallback(const Packet& packet)
{
    Packet internalPacket = packet;
    if(internalPacket.data.getLength() == 2)
    {
        packetData = internalPacket.data[1];
    }
}

void registerCallback()
{
    // TODO: currently have no way of testing this
}

void publish()
{
    Server server;
    server.registerCallback(ecpCallback);
    
    /*//             [version-]  [--------------------Command ID--------------]  [Data Length]  [--------------------Data--------------------]
    char data[] = {0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x08,    0x00, 0x00, 0x00, 0x00 ,0x00, 0x00, 0x00, 0x02};
    ECP::Packet p = ECP::Packet(data);
    server.publish(p, ecpCallback);*/
    ECP::RegisterCommand_Packet p1(2);
    server.publish(p1, ecpCallback);

    //              [version-]  [--------------------Command ID--------------]  [Data Length]  [--Data--]
    char data2[] = {0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x02,    0x69, 0x42};
    ECP::Packet p2 = ECP::Packet(data2);
    server.publish(p2, (ClientHandle)69/*random id that isn't its own so it doesn't fail to send*/);

    assert(packetData == 0x42);

    printTestFinished("ECP",__func__,__LINE__);
}

TestStarter test_createServer((TestFunction)createServer);
TestStarter test_publish((TestFunction)publish);