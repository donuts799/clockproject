#include "Test.hpp"
#include "Date.hpp"
#include <iostream>



void date()
{
    Date::Date testDate;

    assert(true);

    printTestFinished("Date",__func__,__LINE__);
}

void getCurrentDate()
{
    while(true)
    {
        Date::Date date = Date::Date::getCurrentDate();
        std::cout << date.asDateTimeString() << std::endl;
    }
}

void asDateTimeString()
{
    Date::Date testDate;
    testDate.year = 6969;
    testDate.month = Date::Month::JANUARY;
    testDate.monthday = 16;
    testDate.hour = 4;
    testDate.minute = 20;
    testDate.second = 2;
    testDate.millisecond = 69;
    String<50> a("6969-01-16T04:20:02.069");
    String<50> b = testDate.asDateTimeString();
    assert(a == b);

    printTestFinished("Date",__func__,__LINE__);
}

void getWeekdayString()
{
    Date::Date testDate;
    testDate.weekday = Date::Weekday::MONDAY;
    assert(String<15>("Monday") == testDate.getWeekdayString());
    testDate.weekday = Date::Weekday::TUESDAY;
    assert(String<15>("Tuesday") == testDate.getWeekdayString());
    testDate.weekday = Date::Weekday::WEDNESDAY;
    assert(String<15>("Wednesday") == testDate.getWeekdayString());
    testDate.weekday = Date::Weekday::THURSDAY;
    assert(String<15>("Thursday") == testDate.getWeekdayString());
    testDate.weekday = Date::Weekday::FRIDAY;
    assert(String<15>("Friday") == testDate.getWeekdayString());
    testDate.weekday = Date::Weekday::SATURDAY;
    assert(String<15>("Saturday") == testDate.getWeekdayString());
    testDate.weekday = Date::Weekday::SUNDAY;
    assert(String<15>("Sunday") == testDate.getWeekdayString());
    
    Date::Weekday test = static_cast<Date::Weekday>(0);
    assert(test == Date::Weekday::SUNDAY);
    test = static_cast<Date::Weekday>(69);
    assert(test != Date::Weekday::SUNDAY);


    printTestFinished("Date",__func__,__LINE__);
}

void getMonthString()
{
    Date::Date testDate;
    testDate.month = Date::Month::JANUARY;
    assert(String<15>("January") == testDate.getMonthString());
    testDate.month = Date::Month::FEBRUARY;
    assert(String<15>("February") == testDate.getMonthString());
    testDate.month = Date::Month::MARCH;
    assert(String<15>("March") == testDate.getMonthString());
    testDate.month = Date::Month::APRIL;
    assert(String<15>("April") == testDate.getMonthString());
    testDate.month = Date::Month::MAY;
    assert(String<15>("May") == testDate.getMonthString());
    testDate.month = Date::Month::JUNE;
    assert(String<15>("June") == testDate.getMonthString());
    testDate.month = Date::Month::JULY;
    assert(String<15>("July") == testDate.getMonthString());
    testDate.month = Date::Month::AUGUST;
    assert(String<15>("August") == testDate.getMonthString());
    testDate.month = Date::Month::SEPTEMBER;
    assert(String<15>("September") == testDate.getMonthString());
    testDate.month = Date::Month::OCTOBER;
    assert(String<15>("October") == testDate.getMonthString());
    testDate.month = Date::Month::NOVEMBER;
    assert(String<15>("November") == testDate.getMonthString());
    testDate.month = Date::Month::DECEMBER;
    assert(String<15>("December") == testDate.getMonthString());

    printTestFinished("Date",__func__,__LINE__);
}


TestStarter test_date((TestFunction)date);
// TestStarter test_getCurrentDate((TestFunction)getCurrentDate);
TestStarter test_asDateTimeString((TestFunction)asDateTimeString);
TestStarter test_getWeekdayString((TestFunction)getWeekdayString);
TestStarter test_getMonthString((TestFunction)getMonthString);