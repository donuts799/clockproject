#include "Test.hpp"
#include "ECPBaseCommands.hpp"

using namespace ECP;


void create_RegisterCommand_Packet_WithRaw()
{
    //             [version-]  [--------------------Command ID--------------]  [Data Length]  [--Data--]
    char data[] = {0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x08,    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x69};
    ECP::RegisterCommand_Packet registerPacket(data);
    ECP::Packet base(data);
    assert(base == registerPacket);
    assert(registerPacket.getRegisterCommandID() == 0x69);
    // assert(p == 0x42);

    printTestFinished("ECP",__func__,__LINE__);
}

void create_RegisterCommand_Packet_WithBase()
{
    //             [version-]  [--------------------Command ID--------------]  [Data Length]  [--Data--]
    char data[] = {0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x08,    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x69};
    ECP::Packet base(data);
    ECP::RegisterCommand_Packet registerPacket(base);
    assert(base == registerPacket);
    assert(registerPacket.getRegisterCommandID() == 0x69);
    // assert(p == 0x42);

    printTestFinished("ECP",__func__,__LINE__);
}

// getRegisterCommandID tested with the constructor tests.

TestStarter test_create_RegisterCommand_Packet_WithRaw((TestFunction)create_RegisterCommand_Packet_WithRaw);
TestStarter test_create_RegisterCommand_Packet_WithBase((TestFunction)create_RegisterCommand_Packet_WithBase);
