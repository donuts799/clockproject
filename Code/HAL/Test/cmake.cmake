set(hal_test_dir ${hal_dir}/Test)

set(hal_test_sources ${hal_test_sources}
    ${hal_test_dir}/ByteOrder_test.cpp
    ${hal_test_dir}/ECP_test.cpp
    ${hal_test_dir}/ECPServer_test.cpp
    ${hal_test_dir}/ECPBaseCommands_test.cpp
    ${hal_test_dir}/Date_test.cpp
    #${hal_test_dir}/ECPServerAdapter_test.cpp
    ${hal_test_dir}/Math_test.cpp
    ${hal_test_dir}/String_test.cpp
    ${hal_sources}
)