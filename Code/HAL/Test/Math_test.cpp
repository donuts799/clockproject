#include "Test.hpp"
#include "Math.hpp"

void isWithinPercent_uint32_t()
{
    assert(Math::isWithinPercent(100, 91, 10));
    assert(Math::isWithinPercent(100, 109, 10));
    printTestFinished("Math",__func__,__LINE__);
}

void isWithinPercent_uint32_t_invalid()
{
    assert(false == Math::isWithinPercent(100, 90, 10));
    assert(false == Math::isWithinPercent(100, 110, 10));
    printTestFinished("Math",__func__,__LINE__);
}

TestStarter test_isWithinPercent_uint32_t((TestFunction)isWithinPercent_uint32_t);
TestStarter test_isWithinPercent_uint32_t_invalid((TestFunction)isWithinPercent_uint32_t_invalid);