set(hal_dir HAL)

set(hal_sources )

set(hal_includes
    ${hal_dir}/Inc
)

set(hal_test_sources )

set(hal_test_includes 
    ${hal_includes}
)

if(${CMAKE_CXX_BYTE_ORDER} STREQUAL "BIG_ENDIAN")
    message(STATUS "Platform: Big Endian")
    include(${hal_dir}/BigEndian/cmake.cmake)
elseif(${CMAKE_CXX_BYTE_ORDER} STREQUAL "LITTLE_ENDIAN")
    message(STATUS "Platform: Little Endian")
    include(${hal_dir}/LittleEndian/cmake.cmake)
else()
    message(FATAL_ERROR "Cannot determine Endianness!")
endif()

include(${hal_dir}/Common/cmake.cmake)

include(${hal_dir}/Test/cmake.cmake)

if(MINGW)
    include(${hal_dir}/MinGW/cmake.cmake)
elseif(UNIX)
    include(${hal_dir}/Posix/cmake.cmake)
endif()