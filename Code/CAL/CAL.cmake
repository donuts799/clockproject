set(ecp_directory CAL/EventCommunicationProtocol)

set(ecp_sources 
    ${ecp_directory}/ECP.cpp
)

set(ecp_includes
    ${ecp_directory}
)

set(ecp_test_sources
    ${ecp_directory}/ECP_test.cpp
)

message(STATUS "ByteOrder: " ${CMAKE_C_BYTE_ORDER})

set(byte_order_directory CAL/ByteOrder)

set(byte_order_sources
)

# choose file based on Endianness
if(${CMAKE_CXX_BYTE_ORDER} STREQUAL "BIG_ENDIAN")
    set(byte_order_sources ${byte_order_sources} 
        ${byte_order_directory}/ByteOrder_BigEndian.cpp
    )
elseif(${CMAKE_CXX_BYTE_ORDER} STREQUAL "LITTLE_ENDIAN")
    set(byte_order_sources ${byte_order_sources} 
        ${byte_order_directory}/ByteOrder_LittleEndian.cpp
    )
else()
    message(FATAL_ERROR "CANNOT DETERMINE ENDIANNESS")
endif()

set(byte_order_includes
    ${byte_order_directory}
)

set(byte_order_test_sources
    ${byte_order_directory}/ByteOrder_test.cpp
)

set(cal_sources
    ${ecp_sources}
    ${byte_order_sources}
)

set(cal_includes
    ${ecp_includes}
    ${byte_order_includes}
)

set(cal_test_sources
    ${ecp_test_sources}
    ${byte_order_test_sources}
)

add_library(cal STATIC ${cal_sources})

target_include_directories(cal PUBLIC ${cal_includes})


add_library(cal_test STATIC ${cal_test_sources})

target_include_directories(cal_test PUBLIC ${cal_includes} ${test_includes})

target_link_libraries(cal_test cal)