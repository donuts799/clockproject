set(ao_dir AOs)

set(ao_sources 
    ${ao_dir}/DateTick.cpp
    ${ao_dir}/Orchestrator.cpp
    ${ao_dir}/AOs.cpp
    ${ao_dir}/init.cpp
    ${ao_dir}/Standard.cpp
    ${ao_dir}/BaseAO.cpp
    ${ao_dir}/AOEvents.cpp
)


set(ao_includes
    ${ao_dir}
)

set(ao_test_sources )

set(ao_test_includes 
    ${ao_includes}
)
