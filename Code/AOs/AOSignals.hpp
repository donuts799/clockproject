#pragma once
#include "qpcpp.hpp"

enum class AOSignal : enum_t
{
    // Used for internal timeout timers
    TIMEOUT_SIG = QP::Q_USER_SIG, // offset the first signal
    DATE_TICK_SIG,
    TEST_LOG_SIG,
    TEST_STARTED_SIG,
    TEST_INSTANCE_NEXT_SIG,
    TEST_INSTANCE_STARTED_SIG,
    TEST_INSTANCE_FINISHED_SIG,
    TEST_FINISHED_SIG,
    GO_TO_CHILD_SIG,
    GO_TO_NEXT_STATE_SIG,
    MAX_SIG
};