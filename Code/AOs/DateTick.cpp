//.$file${AOs::DateTick.cpp} vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
//
// Model: AOs.qm
// File:  ${AOs::DateTick.cpp}
//
// This code has been generated by QM 5.1.1 <www.state-machine.com/qm/>.
// DO NOT EDIT THIS FILE MANUALLY. All your changes will be lost.
//
// This program is open source software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
//.$endhead${AOs::DateTick.cpp} ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
#include <iostream>
#include "qpcpp.hpp" // QP/C++ framework API
#include "Date.hpp"
#include "DateTick.hpp"
#include "qf_port.hpp"
using namespace QP;

// ask QM to declare the Blinky class ----------------------------------------
//$declare${AOs::DateTick}

// opaque pointer to the Blinky active object --------------------------------
//$define${AOs::AO_DateTick}

// ask QM to define the Blinky class (including the state machine) -----------
//.$skip${QP_VERSION} vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
//. Check for the minimum required QP version
#if (QP_VERSION < 690U) || (QP_VERSION != ((QP_RELEASE^4294967295U) % 0x3E8U))
#error qpcpp version 6.9.0 or higher required
#endif
//.$endskip${QP_VERSION} ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//.$define${AOs::DateTick} vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
//.${AOs::DateTick} ..........................................................
//.${AOs::DateTick::DateTick} ................................................
DateTick::DateTick()
  : TestableAO(&DateTick::initial),
    m_timeEvt(this, (enum_t)AOSignal::TIMEOUT_SIG, 0U)
{}

//.${AOs::DateTick::createTestAO} ............................................
void DateTick::createTestAO(
    void* data,
    int length,
    QP::QEvt const** eventQueue,
    int queueLength,
    enum_t priority)
{
    assert(length >= sizeof(DateTick_Test));

    DateTick_Test* test = (DateTick_Test*)data;
    // initialize in place: https://stackoverflow.com/questions/5806305/is-there-a-way-to-call-constructor-with-class-instance-pointer
    new (test) DateTick_Test();
    test->start(priority, // priority of the active object
        eventQueue, // event queue buffer
        queueLength // the length of the buffer
    );

    return;
}

//.${AOs::DateTick::getEventQueueBuffer} .....................................
QP::QEvt const** const DateTick::getEventQueueBuffer() {
    return queueSto;
}

//.${AOs::DateTick::getEventQueueDim} ........................................
size_t DateTick::getEventQueueDim() {
    return Q_DIM(queueSto);
}

//.${AOs::DateTick::kill} ....................................................
void DateTick::kill() {
    m_timeEvt.disarm();
    stop();
}

//.${AOs::DateTick::getName} .................................................
String<50> DateTick::getName() {
    return "DateTick";
}

//.${AOs::DateTick::destroyTestAO} ...........................................
void DateTick::destroyTestAO(void* data, int length) {
    assert(length >= sizeof(DateTick_Test));

    ((DateTick_Test*)data)->stop();
    ((DateTick_Test*)data)->~DateTick_Test();
}

//.${AOs::DateTick::stop} ....................................................
void DateTick::stop() {
    m_timeEvt.disarm();

    BaseAO::stop();
}

//.${AOs::DateTick::start} ...................................................
void DateTick::start(
    enum_t priority,
    QP::QEvt const** eventQueue,
    int queueLength)
{
    new (this) DateTick();
    QActive::start(priority, eventQueue, queueLength, 0, 0);
}

//.${AOs::DateTick::SM} ......................................................
Q_STATE_DEF(DateTick, initial) {
    //.${AOs::DateTick::SM::initial}
    (void)e; // unused parameter
    m_timeEvt.armX(DATE_TICK_PERIOD_TICKS,
                   DATE_TICK_PERIOD_TICKS);

    QS_FUN_DICTIONARY(&DateTick::publishDate);

    return tran(&publishDate);
}
//.${AOs::DateTick::SM::publishDate} .........................................
Q_STATE_DEF(DateTick, publishDate) {
    QP::QState status_;
    switch (e->sig) {
        //.${AOs::DateTick::SM::publishDate}
        case Q_ENTRY_SIG: {
            Date::Date currentDate = Date::Date::getCurrentDate();
            DateTickEvent* dateEvt = Q_NEW(DateTickEvent, (enum_t)AOSignal::DATE_TICK_SIG);
            dateEvt->now = currentDate;
            QF::publish_(dateEvt);
            status_ = Q_RET_HANDLED;
            break;
        }
        //.${AOs::DateTick::SM::publishDate::(enum_t)AOSignal::TIMEOUT}
        case (enum_t)AOSignal::TIMEOUT_SIG: {
            Date::Date currentDate = Date::Date::getCurrentDate();
            DateTickEvent* dateEvt = Q_NEW(DateTickEvent, (enum_t)AOSignal::DATE_TICK_SIG);
            dateEvt->now = currentDate;
            QF::publish_(dateEvt);
            status_ = Q_RET_HANDLED;
            break;
        }
        default: {
            status_ = super(&top);
            break;
        }
    }
    return status_;
}
//.$enddef${AOs::DateTick} ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

//.$define${AOs::DateTick_Test} vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
//.${AOs::DateTick_Test} .....................................................
//.${AOs::DateTick_Test::DateTick_Test} ......................................
DateTick_Test::DateTick_Test()
: BaseAO(&DateTick_Test::initial)
{}

//.${AOs::DateTick_Test::getName} ............................................
String<50> DateTick_Test::getName() {
    return "DateTick_Test";
}

//.${AOs::DateTick_Test::start} ..............................................
void DateTick_Test::start(
    enum_t priority,
    QP::QEvt const** eventQueue,
    int queueLength)
{
    new (this) DateTick_Test();
    QActive::start(priority, eventQueue, queueLength, 0, 0);
}

//.${AOs::DateTick_Test::SM} .................................................
Q_STATE_DEF(DateTick_Test, initial) {
    //.${AOs::DateTick_Test::SM::initial}
    this->subscribe((enum_t)AOSignal::TEST_INSTANCE_NEXT_SIG);

    QEvt* evt = Q_NEW(QP::QEvt, (enum_t)AOSignal::TEST_STARTED_SIG);
    QF::publish_(evt);
    return tran(&StartTesting);
}
//.${AOs::DateTick_Test::SM::StartTesting} ...................................
Q_STATE_DEF(DateTick_Test, StartTesting) {
    QP::QState status_;
    switch (e->sig) {
        //.${AOs::DateTick_Test::SM::StartTesting::(enum_t)AOSignal::TEST_INSTANCE_~}
        case (enum_t)AOSignal::TEST_INSTANCE_NEXT_SIG: {
            status_ = tran(&TestDateTickPublishes);
            break;
        }
        default: {
            status_ = super(&top);
            break;
        }
    }
    return status_;
}
//.${AOs::DateTick_Test::SM::TestDateTickPublishes} ..........................
Q_STATE_DEF(DateTick_Test, TestDateTickPublishes) {
    QP::QState status_;
    switch (e->sig) {
        //.${AOs::DateTick_Test::SM::TestDateTickPublishes}
        case Q_ENTRY_SIG: {
            this->subscribe((enum_t)AOSignal::DATE_TICK_SIG);
            TestInstanceStartedEvent* evt = Q_NEW(TestInstanceStartedEvent, (enum_t)AOSignal::TEST_INSTANCE_STARTED_SIG);
            evt->name.set("TestDateTickPublishes");
            QF::publish_(evt);
            status_ = Q_RET_HANDLED;
            break;
        }
        //.${AOs::DateTick_Test::SM::TestDateTickPublishes}
        case Q_EXIT_SIG: {
            this->unsubscribe((enum_t)AOSignal::DATE_TICK_SIG);
            status_ = Q_RET_HANDLED;
            break;
        }
        //.${AOs::DateTick_Test::SM::TestDateTickPubl~::(enum_t)AOSignal::DATE_TICK}
        case (enum_t)AOSignal::DATE_TICK_SIG: {
            TestFinishedEvent* evt = Q_NEW(TestFinishedEvent, (enum_t)AOSignal::TEST_INSTANCE_FINISHED_SIG);
            evt->status = TestFinishedStatus::SUCCEDED;
            QF::publish_(evt);
            status_ = tran(&WaitForOrchestrator1);
            break;
        }
        default: {
            status_ = super(&top);
            break;
        }
    }
    return status_;
}
//.${AOs::DateTick_Test::SM::TestDateTickDuration} ...........................
Q_STATE_DEF(DateTick_Test, TestDateTickDuration) {
    QP::QState status_;
    switch (e->sig) {
        //.${AOs::DateTick_Test::SM::TestDateTickDuration}
        case Q_ENTRY_SIG: {
            this->subscribe((enum_t)AOSignal::DATE_TICK_SIG);

            QEvt* evt = Q_NEW(QP::QEvt, (enum_t)AOSignal::GO_TO_CHILD_SIG);
            POST(evt, 0);

            TestInstanceStartedEvent* startedEvt = Q_NEW(TestInstanceStartedEvent, (enum_t)AOSignal::TEST_INSTANCE_STARTED_SIG);
            startedEvt->name.set("TestDateTickVerifyPeriod");
            QF::publish_(startedEvt);
            status_ = Q_RET_HANDLED;
            break;
        }
        //.${AOs::DateTick_Test::SM::TestDateTickDura~::(enum_t)AOSignal::GO_TO_CHILD}
        case (enum_t)AOSignal::GO_TO_CHILD_SIG: {
            status_ = tran(&WaitForTestableDate);
            break;
        }
        default: {
            status_ = super(&top);
            break;
        }
    }
    return status_;
}
//.${AOs::DateTick_Test::SM::TestDateTickDura~::WaitForTestableDate} .........
Q_STATE_DEF(DateTick_Test, WaitForTestableDate) {
    QP::QState status_;
    switch (e->sig) {
        //.${AOs::DateTick_Test::SM::TestDateTickDura~::WaitForTestableD~::(enum_t)AOSignal::DATE_TICK}
        case (enum_t)AOSignal::DATE_TICK_SIG: {
            //.${AOs::DateTick_Test::SM::TestDateTickDura~::WaitForTestableD~::(enum_t)AOSignal~::[lessthanhalfasecondelapsed]}
            if (((DateTickEvent*)e)->now.millisecond < (MS_PER_SECOND *.9)
                /*Wait for less than half a second has elapsed to give a buffer for the next tick to not overflow*/)
            {
                dateStorage = ((DateTickEvent*)e)->now;
                status_ = tran(&WaitForSecondDate);
            }
            //.${AOs::DateTick_Test::SM::TestDateTickDura~::WaitForTestableD~::(enum_t)AOSignal~::[else]}
            else {
                status_ = Q_RET_HANDLED;
            }
            break;
        }
        default: {
            status_ = super(&TestDateTickDuration);
            break;
        }
    }
    return status_;
}
//.${AOs::DateTick_Test::SM::TestDateTickDura~::WaitForSecondDate} ...........
Q_STATE_DEF(DateTick_Test, WaitForSecondDate) {
    QP::QState status_;
    switch (e->sig) {
        //.${AOs::DateTick_Test::SM::TestDateTickDura~::WaitForSecondDat~::(enum_t)AOSignal::DATE_TICK}
        case (enum_t)AOSignal::DATE_TICK_SIG: {
            DateTickEvent* tickEvt = (DateTickEvent*)e;

            TestFinishedEvent* evt = Q_NEW(TestFinishedEvent, (enum_t)AOSignal::TEST_INSTANCE_FINISHED_SIG);

            if(Math::isWithinPercent(DATE_TICK_PERIOD_MS, tickEvt->now.millisecond - dateStorage.millisecond , 20))
            {
                evt->status = TestFinishedStatus::SUCCEDED;
            }
            else
            {
                evt->status = TestFinishedStatus::FAILED;
            }
            QF::publish_(evt);
            status_ = tran(&WaitForOrchestrator2);
            break;
        }
        default: {
            status_ = super(&TestDateTickDuration);
            break;
        }
    }
    return status_;
}
//.${AOs::DateTick_Test::SM::Finished} .......................................
Q_STATE_DEF(DateTick_Test, Finished) {
    QP::QState status_;
    switch (e->sig) {
        //.${AOs::DateTick_Test::SM::Finished}
        case Q_ENTRY_SIG: {
            QEvt* evt = Q_NEW(QEvt, (enum_t)AOSignal::TEST_FINISHED_SIG);
            QF::publish_(evt);
            status_ = Q_RET_HANDLED;
            break;
        }
        default: {
            status_ = super(&top);
            break;
        }
    }
    return status_;
}
//.${AOs::DateTick_Test::SM::WaitForOrchestrator1} ...........................
Q_STATE_DEF(DateTick_Test, WaitForOrchestrator1) {
    QP::QState status_;
    switch (e->sig) {
        //.${AOs::DateTick_Test::SM::WaitForOrchestra~::(enum_t)AOSignal::TEST_INSTANCE_~}
        case (enum_t)AOSignal::TEST_INSTANCE_NEXT_SIG: {
            status_ = tran(&TestDateTickDuration);
            break;
        }
        default: {
            status_ = super(&top);
            break;
        }
    }
    return status_;
}
//.${AOs::DateTick_Test::SM::WaitForOrchestrator2} ...........................
Q_STATE_DEF(DateTick_Test, WaitForOrchestrator2) {
    QP::QState status_;
    switch (e->sig) {
        //.${AOs::DateTick_Test::SM::WaitForOrchestra~::(enum_t)AOSignal::TEST_INSTANCE_~}
        case (enum_t)AOSignal::TEST_INSTANCE_NEXT_SIG: {
            status_ = tran(&Finished);
            break;
        }
        default: {
            status_ = super(&top);
            break;
        }
    }
    return status_;
}
//.$enddef${AOs::DateTick_Test} ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
