#include "init.hpp"
#include "Orchestrator.hpp"

int init() {
    Orchestrator orchestratorAO;

    QF::init();  // initialize the framework

    // Initialize the orchestrator, it will initialize everything else
    orchestratorAO.start((enum_t)AOPriority::ORCHESTRATOR,
        orchestratorAO.orchestrator_queueSto,
        Q_DIM(orchestratorAO.orchestrator_queueSto),
        (void*) 0, 0U
    );
 
    return QF::run(); // let the framework run the application
}