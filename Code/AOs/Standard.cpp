#include "Standard.hpp"
#include <iostream>
#include "qpcpp.hpp"

using namespace QP;

void QF::onStartup(void) {
}
void QP::QF::onCleanup(void) {}
void QP::QF_onClockTick(void) {
    QF::TICK_X(0U, 0); // QF clock tick processing for rate 0
}

void Q_onAssert(char const * const module, int loc) {
    std::cerr << "Assertion failed in " <<  module << ":" << loc << std::endl;
    exit(-1);
}