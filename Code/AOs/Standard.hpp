#pragma once
#include <string>
#include "Utilities.hpp"
#include "AOEvents.hpp"
#include "AOPriorities.hpp"
#include "AOSignals.hpp"
#include "BaseAO.hpp"

#define assert(value) ((value == true) ? "" : throw "Assertion Failed!")

const uint32_t TICKS_PER_SECOND = 100;
const uint32_t MS_PER_SECOND = 1000;

const size_t STANDARD_QUEUE_BUFFER_LENGTH   = 10;
const size_t TEST_QUEUE_BUFFER_LENGTH       = 10;

using namespace Utilities;