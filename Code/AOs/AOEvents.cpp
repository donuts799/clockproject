#include "AOEvents.hpp"
#include "String.hpp"

using namespace Utilities;

String<20> testStatusToString(TestFinishedStatus status)
{
    switch(status)
    {
        case TestFinishedStatus::FAILED:
        {
            return String<20>("Failed");
        }
        case TestFinishedStatus::SUCCEDED:
        {
            return String<20>("Succeded");
        }
        default:
        {
            return String<20>("Unknown");
        }
    }
}