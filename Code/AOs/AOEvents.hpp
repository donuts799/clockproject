#pragma once
#include <string>
#include "qpcpp.hpp"
#include "Utilities.hpp"
#include "Date.hpp"

using namespace Utilities;

struct DateTickEvent : QP::QEvt
{
    Date::Date now;
};

enum class TestFinishedStatus : enum_t
{
    UNKNOWN = 0,
    SUCCEDED,
    FAILED
};
struct TestFinishedEvent : QP::QEvt
{
    TestFinishedStatus status = TestFinishedStatus::UNKNOWN;
};
String<20> testStatusToString(TestFinishedStatus);

enum class LogPriority : enum_t
{
    INFORMATIONAL,
    WARNING,
    ERROR
};
struct LogEvent : QP::QEvt
{
public:
    LogPriority prio;
    String<100> text;
};

struct TestInstanceStartedEvent : QP::QEvt
{
    String<50> name;
};