#pragma once

enum class AOPriority
{
    ORCHESTRATOR    = 2,
    TEST            = 1
};