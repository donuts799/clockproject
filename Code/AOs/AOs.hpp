//.$file${AOs::AOs.hpp} vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
//
// Model: AOs.qm
// File:  ${AOs::AOs.hpp}
//
// This code has been generated by QM 5.1.0 <www.state-machine.com/qm/>.
// DO NOT EDIT THIS FILE MANUALLY. All your changes will be lost.
//
// This program is open source software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
//.$endhead${AOs::AOs.hpp} ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
#pragma once
#ifndef AOS_HPP
#include "BaseAO.hpp"
#include "DateTick.hpp"
#include "qpcpp.hpp"

using namespace QP;
extern const size_t ACTIVE_OBJECTS_COUNT;
extern TestableAO* activeObjects[];
extern const uint64_t aoDefaultRunMask;

#endif